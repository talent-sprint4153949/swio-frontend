import React, { useState, useEffect } from "react";
import "./Trans.css"; // Add CSS file for styling

const ViewTransactionsPage = () => {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    fetchTransactions();
  }, []); // Fetch transactions when the component mounts

  const fetchTransactions = async () => {
    try {
      const response = await fetch("https://swio-backend.onrender.com/payment");
      if (response.ok) {
        const data = await response.json();
        setTransactions(data);
      } else {
        console.error("Failed to fetch transactions");
      }
    } catch (error) {
      console.error("Error fetching transactions:", error);
    }
  };

  return (
    <div className="transactions-container" >
      <h2 className="page-title">View Transactions</h2>
      <div className="transactions-grid">
        {transactions.map((transaction) => (
          <TransactionCard key={transaction.TransactionId} transaction={transaction} />
        ))}
      </div>
    </div>
  );
};

const TransactionCard = ({ transaction }) => {
  return (
    <div className="transaction-card">
      <div className="transaction-details">
        <h3 className="transaction-name">{transaction.Name}</h3>
        <p className="transaction-amount">Amount: ${transaction.Amount}</p>
      </div>
    </div>
  );
};

export default ViewTransactionsPage;
