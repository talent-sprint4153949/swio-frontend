import React, { useState } from "react";
import { FormGroup } from "reactstrap";
import { loadStripe } from '@stripe/stripe-js';
import "./Payment.css"; // Import CSS file for styling

const Payment = () => {
    const [data, setData] = useState({ Name: '', Amount: '' });
    const [errors, setErrors] = useState({ name: '', amount: '' });

    const validateForm = () => {
        const errors = {};
        if (!data.Name) {
            errors.name = "Name is Required";
        }
        if (!data.Amount) {
            errors.amount = "Amount is Required";
        }
        setErrors(errors);
        return Object.keys(errors).length === 0;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (validateForm) {
            const stripe = await loadStripe("pk_test_51PIaRrSB1kF7GRyR2R07HY6ChKjwN5w3E9MPHG61MHuD0Z4IapUR1XXyV0C3kUxuaWgq9E6LU2cb0FLgPwtPdpXZ00O1SQc0KM");
            const body = {
                Transactions: [{ Name: data.Name, Price: data.Amount }]
            }
            const headers = {
                "Content-Type": "application/json",
            }
            const response = await fetch("https://swio-backend.onrender.com/api/create-checkout-session", {
                method: "POST",
                headers: headers,
                body: JSON.stringify(body)
            })
            const session = await response.json();

            const result = stripe.redirectToCheckout({
                sessionId: session.id
            })
            if (result.error) {
                console.log(result.error);
            }
        }
    }

    return (
        <div className="payment-container">
            <h5 className="payment-title">Payment</h5>
            <form className="payment-form">
                <FormGroup>
                    <label className="payment-label">Name</label>
                    <input
                        type='text'
                        placeholder="Enter Your Name"
                        id="Name"
                        value={data.Name}
                        onChange={(e) => setData({ ...data, Name: e.target.value })}
                        className="payment-input"
                    />
                    {errors.name && <div className="error-message">{errors.name}</div>}
                    <label className="payment-label">Amount</label>
                    <input
                        type='text'
                        placeholder="Enter Your Amount"
                        id="Amount"
                        value={data.Amount}
                        onChange={(e) => setData({ ...data, Amount: e.target.value })}
                        className="payment-input"
                    />
                    {errors.amount && <div className="error-message">{errors.amount}</div>}
                </FormGroup>
                <button type="submit" onClick={(e) => handleSubmit(e)} className="submit-button">Submit</button>
            </form>
        </div>
    )
}

export default Payment;
