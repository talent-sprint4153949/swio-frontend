import './App.css';
import NavBar from './Component/NavBar';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Component/Home';
import Payment from './Pages/Payment';
import Transaction from './Pages/Transaction';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/payment" element={<Payment />}/>
            <Route  path="/trans" element={<Transaction />}/>
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
