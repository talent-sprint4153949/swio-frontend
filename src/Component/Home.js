import React from 'react';

const Home = () => {
  const styles = {
    body: {
      fontFamily: 'Helvetica Neue, sans-serif',
      margin: 0,
      padding: 0,
      background: 'linear-gradient(45deg, #ff9a9e, #fad0c4, #fad0c4, #fbc2eb, #a18cd1)',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    header: {
      width: '100%',
      color: 'white',
      backgroundColor: 'black',
      boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
      marginBottom: '20px',
    },
    nav: {
      maxWidth: '1200px',
      margin: '0 auto',
      padding: '20px',
    },
    navList: {
      listStyle: 'none',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 0,
      margin: 0,
    },
    navListImg: {
      width: '50px',
      height: '50px',
      overflow: 'hidden',
    },
    navListLi: {
      margin: '0 10px',
    },
    navListA: {
      textDecoration: 'none',
      color: 'white',
      fontSize: '1.2em',
    },
    pageTitle: {
      fontSize: '2.5em',
      color: '#333',
      marginBottom: '20px',
    },
    // Add more styles as needed
  };

  return (
    <div style={styles.body}>
      <header style={styles.header}>
        <nav style={styles.nav}>
          <ul style={styles.navList}>
            <li style={styles.navListLi}>
             Swio
            </li>
            <li style={styles.navListLi}>
              <a href="/" style={styles.navListA}>Home</a>
            </li>
            <li style={styles.navListLi}>
              <a href="/about" style={styles.navListA}>About</a>
            </li>
            {/* Add more navigation links */}
          </ul>
        </nav>
      </header>
      <h2 style={styles.pageTitle}>Welcome to Our Website!</h2>
      {/* Add more content */}
    </div>
  );
};

export default Home;
